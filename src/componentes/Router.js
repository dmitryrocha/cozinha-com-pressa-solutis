import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import App from "../App";
import Contato from "./Contato";
import Receita from "./Receita";

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" component={App} exact />
      <Route path="/receita/:id" component={Receita} />
      <Route path="/contato" component={Contato} />
    </Switch>
  </BrowserRouter>
);

export default Router;