import React from "react";
import { Link } from "react-router-dom";

const API_KEY = "47afd3202cbf4a5b8b9fc62972920291";

class Receita extends React.Component {
  state = {
    receitaAtiva: [],
  };

  componentDidMount = async () => {
    const idReceita = this.props.location.state.receita;

    const req = await fetch(
      "https://api.spoonacular.com/recipes/" +
        idReceita +
        "/information?includeNutrition=false&apiKey=" +
        API_KEY
    );
    //const api_call = await fetch("https://api.spoonacular.com/recipes/716429/information?includeNutrition=false&apiKey=47afd3202cbf4a5b8b9fc62972920291");

    const res = await req.json();
    //this.setState({receitas: data.results});
    this.setState({ receitaAtiva: res });
  };

  render() {
    const receita = this.state.receitaAtiva;
    console.log(receita);
    return (
      <div className="container">
        {this.state.receitaAtiva.length !== 0 && (
          <div className="active-recipe">
            <img
              className="active-recipe__img"
              src={receita.image}
              alt={receita.title}
            />
            <h3 className="active-recipe__title">{receita.title}</h3>
            <h4 className="active-recipe__publisher">
              <span>{receita.creditsText}</span>
            </h4>
            <p className="active-recipe__website">
              URL:{" "}
              <span>
                <a href={receita.sourceUrl}>{receita.sourceUrl}</a>
              </span>
            </p>
            <p>{receita.summary}</p>
            <button className="active-recipe__button">
              <Link to="/">Voltar para o início</Link>
            </button>
            <button className="active-recipe__button">
              <a href={receita.sourceUrl} target="blank">Abrir a receita</a>
            </button>
          </div>
        )}
      </div>

    );
  }
}

export default Receita;
