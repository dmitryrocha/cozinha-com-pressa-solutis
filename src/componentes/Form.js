import React from "react"

const Form = props => (
    <form onSubmit={props.getReceita} style={{marginBottom:"2rem"}}>
        <input className="form__input" type="text" name="nomeReceita"/>
        <button className="form__button">Buscar</button>
    </form>
);

export default Form;
