import React from "react";
import { Link } from "react-router-dom";

const Receitas = props => (
  <div className="container">
    <div className="row">
      {props.receitas.map((receita) => {
        return (
          <div key={receita.id} className="col-md-4" style={{ marginBottom:"2rem"}}>
            <div className="recipes__box">
              <img
                className="recipe__box-img"
                src={receita.image}
                alt={receita.title}
              ></img>
              <div className="recipe__text">
                <h5 className="recipes__title">
                {receita.title.length<20 ? receita.title : receita.title.substring(0,27)+"..."}</h5>
                </div>
                <button className="recipe_buttons">
                <Link to={{ pathname: '/receita/'+receita.id, state: {receita:receita.id}}}>Ver receita</Link>
                </button>
            </div>
          </div>
        );
      })}
    </div>
  </div>
);

export default Receitas;
