import React from "react";
import '../App.css';
import NavBar from "../NavBar";
import Headers from "../Headers";
import { Form, FormGroup, Label, Input } from "reactstrap";
import { Link } from "react-router-dom";

const Contato = (props) => {
  return (
      

      <div className="App">
        <NavBar />
        <Headers />
        <div className="container">
        <Form style={{alignContent: "center"}}>
          <FormGroup>
            <h3>Envie sua receita</h3>
          </FormGroup>
          <FormGroup>
            <Label for="exampleEmail">E-mail</Label>
            <Input
              type="email"
              name="email"
              id="exampleEmail"
              
            />
          </FormGroup>
          <Label for="exampleEmail">Nome completo</Label>
          <Input
            type="text"
            name="nome"
            id="nome"
            
          />
          <FormGroup></FormGroup>
          <FormGroup>
            <Label for="exampleText">Receita</Label>
            <Input type="textarea" name="text" id="receita" />
          </FormGroup>
          <button className="active-recipe__button">Submit</button>
          <button className="active-recipe__button">
            <Link to="/">Voltar para o início</Link>
          </button>
        </Form>
      </div>
    </div>
  );
};

export default Contato;
