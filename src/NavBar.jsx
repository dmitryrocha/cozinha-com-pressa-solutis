import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText
} from 'reactstrap';

const NavBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">Cozinha com Pressa</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/contato">Contato</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://solutis.com.br/">Solutis</NavLink>
            </NavItem>
          </Nav>
          <NavbarText>powered by Spoonacular</NavbarText>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavBar;