import React, { useState } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';
import imagem1 from "./images/panelayakialt.jpeg";
import imagem2 from "./images/cortatomatealt.jpg";
import imagem3 from "./images/cenourasalt.jpg";
import imagem4 from "./images/carnealt.jpg";
import imagem5 from "./images/ovoalt.jpg"
import "./App.css";

const items = [
  {
    src: imagem1,
    altText: 'Mulher segurando uma panela',
    caption: 'Comida rápida'
  },
  {
    src: imagem2,
    altText: 'Verduras sendo cortadas',
    caption: 'Saborosa'
  },
  {
    src: imagem3,
    altText: 'cenouras temperadas',
    caption: 'Saudável'

  },
  {
    src: imagem4,
    altText: 'Diversos cortes de carnes',
    caption: 'Para todos os gostos'
  },
  {
    src: imagem5,
    altText: 'Ovos estrelados',
    caption: 'E pressas'
  }
];

const Header = (props) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img className="carousel-inner" src={item.src} alt={item.altText} />
        <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
      </CarouselItem>
    );
  });

  return (
    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
    >
      <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
      {slides}
      <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
    </Carousel>
  );
}

export default Header;