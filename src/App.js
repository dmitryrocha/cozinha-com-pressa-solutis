import React, { Component } from 'react';
import './App.css';
import Form from "./componentes/Form";
import Receitas from "./componentes/Receitas";
import NavBar from "./NavBar";
import Headers from "./Headers";


const API_KEY = "47afd3202cbf4a5b8b9fc62972920291";

class App extends Component {

  state = {
    receitas: []
  }

  getReceita = async (e) => {
    let nomeReceita = "";
    
    nomeReceita = e.target.elements.nomeReceita.value;

    e.preventDefault();
    const api_call = await fetch("https://api.spoonacular.com/recipes/complexSearch?query="+nomeReceita+"&maxReadyTime=20&number=9&apiKey="+API_KEY);
    
    const data = await api_call.json();
    console.log(data);
    this.setState({receitas: data.results});
    console.log(this.state.receitas);
  }


  componentDidMount = () => {
    const json = localStorage.getItem("receitas");
    const receitas = JSON.parse(json);
    this.setState({receitas: receitas});
  }

  componentDidUpdate = () => {
    const receitas = JSON.stringify(this.state.receitas);
    localStorage.setItem("receitas", receitas);
  }

  render() {
    return (
      <div className="App">
        <NavBar />
        <Headers />
        <Form   getReceita = {this.getReceita}/>
        <Receitas receitas={this.state.receitas} />
      </div>
    );
  }
}

export default App;